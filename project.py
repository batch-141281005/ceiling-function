class Node:
    def __init__(self, num):
        self.num = num
        self.left = None
        self.right = None
class Tree:
    def __init__(self):
        self.root = None
    def insert(self, num):
        self.root = self.insertNode(self.root, num)
    def insertNode(self, root, num):
        if root is None:
            return Node(num)
        if root.num > num:
            root.left = self.insertNode(root.left, num)
        else:
            root.right = self.insertNode(root.right, num)
        return root

    def isSimilar(self, otherTree):
        return self.isSimilarTree(self.root, otherTree.root)

    def isSimilarTree(self, root1, root2):
        if root1 is None and root2 is None:
            return True
        if root1 is None:
            return False  
        if root2 is None:
            return False
        isLeftSubtreeSimilar = self.isSimilarTree(root1.left, root2.left)
        isRightSubtreeSimilar = self.isSimilarTree(root1.right, root2.right)
        return isLeftSubtreeSimilar and isRightSubtreeSimilar

def parse_arguments():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('prototypes', type=int, help='Number of trees')
    parser.add_argument('layers', type=int, help='Number of levels')
    parser.add_argument('levels', nargs='+', type=int, help='Levels of each tree')
    return parser.parse_args()

def count_unique_trees(prototypes, layers, levels):
    bstMaps = {}
    for i in range(prototypes):
        levels_in_Tree = levels[i*layers:(i+1)*layers]
        bst = Tree()
        for level in levels_in_Tree:
            bst.insert(level)
        isUnique = True
        for bstId in bstMaps:
            if bstMaps [bstId].isSimilar(bst):
                isUnique = False
                break
        if isUnique:
            bstMaps[i] = bst
    return(len(bstMaps))
if __name__ == '__main__':
    args = parse_arguments()
    prototypes, layers, levels = args.prototypes, args.layers, args.levels
    print(count_unique_trees(prototypes, layers, levels))
